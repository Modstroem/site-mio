﻿using MIO.WebCrmApi1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace MIO.Pages
{
    
    public partial class ComunicationsLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                Session["LoadingComunications"] = "15";
                Session["LoadingImageComunications"] = null;
            }


            if (Session["LoadingComunications2"] == null)
            {

                Random r = new Random();
                int i = r.Next(1, 18);
                Image1.ImageUrl = "~/Images/Gif/" + i + ".gif";
                Session["LoadingComunications2"] = "Is set";
                Label1.Visible = true;
            }
        }
        public void LoadLog()
        {


            List<OrderHistory> mainlist = new List<OrderHistory>();


            string DGKConnection = "Data Source=SPSQL004;Initial Catalog=MODSTROEM_LOGS;Persist Security Info=True;User ID=API;password=Duppie36#;";

            SqlConnection conn = new SqlConnection(DGKConnection);
            SqlDataReader reader = null;
            DataTable dataTable = null;

            SqlCommand cmd = new SqlCommand(@"SELECT TOP (1000)
        CONVERT(datetime, COMMUNICATION_TIME, 101) as Dato
      ,[ORIGIN_IP]
      ,[CUSTOMER_LBNR]
      , COMMUNICATION_DESTINATION
  FROM[MODSTROEM_LOGS].[dbo].[COMMUNICATION_LOG] where[ORIGIN_IP] like '%msdk%' and[ID_COMMUNICATION_TYPE] = 15  order by[ID_COMMUNICATION] desc", conn);

            cmd.CommandType = CommandType.Text;
            conn.Open();
            reader = cmd.ExecuteReader();
            dataTable = new DataTable();
            dataTable.Load(reader);
            reader.Close();
            conn.Close();



            foreach (DataRow row in dataTable.Rows)
            {
                try
                {
                    string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();

                    OrderHistory oh = new OrderHistory();
                    DateTime dt = DateTime.Parse(fields[0]).AddDays(-2);
                    oh.DateFinalized = dt.ToShortDateString() + " " + dt.ToShortTimeString();
                    oh.SellsInitials = fields[1];
                    oh.CustomerNumber = fields[2];

                    string[] ids = fields[3].ToString().Split(new string[] { "=" }, StringSplitOptions.None);
                    if (ids.Count() == 3)
                    {
                        string[] pp = LoadOrderLog(ids[2]).ToString().Split(new string[] { "¤" }, StringSplitOptions.None);
                        oh.PruductName = pp[0];
                        oh.OrderNumber = pp[1];
                    }
                    if (ids.Count() == 2)
                    {
                        string r = LoadOrderLog(ids[1]).ToString();
                        if (r == "")
                        {

                        }
                        string[] pp = LoadOrderLog(ids[1]).ToString().Split(new string[] { "¤" }, StringSplitOptions.None);
                        oh.PruductName = pp[0];
                        oh.OrderNumber = pp[1];
                    }
                    // result += " Order nr: " + fields[0] + " Produkt :" + fields[1] + Environment.NewLine;

                    mainlist.Add(oh);

                }
                catch (Exception ex)
                {
                    Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif";
                }
            }



            GridView_Products.DataSource = mainlist;
            GridView_Products.DataBind();
            GridStyle(GridView_Products);

            //result += Environment.NewLine;

            // result = result;

        }


        int rowNumber = 0;
        int SellsInitials = 1;
        int DateFinalized = 2;
        int OrderNumber = 3;
        int CustomerNumber = 4;
        //int CustomerName = 5;
        int PruductName = 5;
        //int OrderStatus = 7;

        public int BusinessDaysUntil(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                   1 + ((endD - startD).TotalDays * 5 -
                   (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return (int)calcBusinessDays;
        }

        public string LoadOrderLog(string id)
        {


            string result = "";


            string DGKConnection = "Data Source=SPSQL004;Initial Catalog=DGK_PROD;Persist Security Info=True;User ID=API;password=Duppie36#;";

            SqlConnection conn = new SqlConnection(DGKConnection);
            SqlDataReader reader = null;
            DataTable dataTable = null;

            SqlCommand cmd = new SqlCommand(@" SELECT OL.[Name], O.OrderNumber FROM [DGK_PROD].[dbo].[TeaCommerce_Order] O
LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_OrderLine]  OL on O.Id = OL.OrderId
where O.Id = '" + id + "'", conn);

            cmd.CommandType = CommandType.Text;
            conn.Open();
            reader = cmd.ExecuteReader();
            dataTable = new DataTable();
            dataTable.Load(reader);
            reader.Close();
            conn.Close();
            string ordernr = "";

            foreach (DataRow row in dataTable.Rows)
            {
                try
                {
                    string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();

                    result += " Produkt : " + fields[0] + Environment.NewLine;
                    if (ordernr == "")
                    {
                        ordernr = fields[1];
                    }


                }
                catch (Exception ex)
                {
                    Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif";
                }
            }

            result += "¤" + ordernr;

            return result;

        }

        public void GridStyle(GridView gw)
        {
            try
            {
                gw.HeaderRow.Cells[rowNumber].Text = "Number";
                gw.HeaderRow.Cells[SellsInitials].Text = "Sells Initials";
                gw.HeaderRow.Cells[DateFinalized].Text = "Date";
                gw.HeaderRow.Cells[OrderNumber].Text = "Order Number";
                gw.HeaderRow.Cells[CustomerNumber].Text = "Customer Number";
                // gw.HeaderRow.Cells[CustomerName].Text = "Customer Name";
                gw.HeaderRow.Cells[PruductName].Text = "Status";
                //  gw.HeaderRow.Cells[OrderStatus].Text = "Product";

                gw.HeaderRow.Cells[SellsInitials].Width = 80;
                gw.HeaderRow.Cells[DateFinalized].Width = 120;
                gw.HeaderRow.Cells[OrderNumber].Width = 80;
                gw.HeaderRow.Cells[CustomerNumber].Width = 60;
                //    gw.HeaderRow.Cells[CustomerName].Width = 100;
                gw.HeaderRow.Cells[PruductName].Width = 250;
                //   gw.HeaderRow.Cells[OrderStatus].Width = 250;





                for (int i = 0; i <= gw.Rows.Count - 1; i++)
                {


                    gw.Rows[i].Cells[rowNumber].Visible = false;
                    gw.HeaderRow.Cells[rowNumber].Visible = false;

                    gw.HeaderRow.Cells[SellsInitials].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[DateFinalized].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[OrderNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[CustomerNumber].HorizontalAlign = HorizontalAlign.Center;
                    //    gw.HeaderRow.Cells[CustomerName].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[PruductName].HorizontalAlign = HorizontalAlign.Left;
                    //   gw.HeaderRow.Cells[OrderStatus].HorizontalAlign = HorizontalAlign.Left;


                    gw.Rows[i].Cells[SellsInitials].Width = 80;
                    gw.Rows[i].Cells[DateFinalized].Width = 120;
                    gw.Rows[i].Cells[OrderNumber].Width = 60;
                    gw.Rows[i].Cells[CustomerNumber].Width = 60;
                    //    gw.Rows[i].Cells[CustomerName].Width = 100;
                    gw.Rows[i].Cells[PruductName].Width = 250;
                    //   gw.Rows[i].Cells[OrderStatus].Width = 250;


                    try
                    {
                        if (BusinessDaysUntil(DateTime.Parse(gw.Rows[i].Cells[2].Text), DateTime.Now) >= 20)
                        {
                            if (gw.Rows[i].Cells[PruductName].Text.Contains("Afventer kundens accept") == true)
                            {
                                gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                                gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                            }
                        }


                        if (gw.Rows[i].Cells[PruductName].Text.Contains("Unable to complete") == true)
                        {
                            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                        }

                    }
                    catch { }


                    gw.Rows[i].Cells[SellsInitials].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[DateFinalized].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[OrderNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[CustomerNumber].HorizontalAlign = HorizontalAlign.Center;
                    // gw.Rows[i].Cells[CustomerName].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[PruductName].HorizontalAlign = HorizontalAlign.Left;
                    //  gw.Rows[i].Cells[OrderStatus].HorizontalAlign = HorizontalAlign.Left;


                }
            }
            catch (Exception ex)
            {
                Session["Error"] += ex.Message + Environment.NewLine + ex.StackTrace;
            }

            Session["LoadingComunications"] = "0";
        }

        private DateTime ConvertToDate(string customDate)
        {
            DateTime dt = DateTime.MinValue;


            try
            {
                string date = customDate;

                if (customDate.Contains(".")) { date = customDate.Replace(".", ""); }
                if (customDate.Contains("-")) { date = customDate.Replace("-", ""); }


                string YYYY = date[0].ToString() + date[1].ToString() + date[2].ToString() + date[3].ToString();
                string MM = date[4].ToString() + date[5].ToString();
                string DD = date[6].ToString() + date[7].ToString();
                dt = DateTime.Parse(YYYY + "-" + MM + "-" + DD);
            }
            catch { }

            if (dt == DateTime.MinValue)
            {
                try
                {
                    dt = DateTime.Parse(customDate);
                }
                catch { }
            }

            return dt;
        }



        protected void GridView_Products_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GridView_Products_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public class OrderHistory
        {
            public int rowId { get; set; }
            public string SellsInitials { get; set; }
            public string DateFinalized { get; set; }
            public string OrderNumber { get; set; }
            public string CustomerNumber { get; set; }
            // public string CustomerName { get; set; }
            // public string OrderStatus { get; set; }
            public string PruductName { get; set; }


            public OrderHistory()
            {

            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (Session["LoadingComunications"].ToString() == "0")
            {
                Timer1.Enabled = false;
                Label1.Text = "";
                Image1.ImageUrl = "";
                Image1.Visible = false;
                Label1.Visible = false;
                this.GridView_Products.Visible = true;

            }
            else
            {
                //int f = Convert.ToInt32(Session["LoadingComunications"].ToString());
                //f = f - 1;
                //Session["LoadingComunications"] = f.ToString();
               

                //Label1.Text = "Wait";
                try
                {
                    if (Session["LoadingImageComunications"] == null)
                    {
                        this.GridView_Products.Visible = false;

                        Random r = new Random();
                        int i = r.Next(1, 18);
                        Image1.ImageUrl = "~/Images/Gif/" + i + ".gif";
                        Session["LoadingImageComunications"] = "Is set";
                        Label1.Visible = true;
                        LoadLog();
                    }
                }
                catch { Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif"; }

            }




        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Panel2.Visible = false;
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (TextBox1.Text.Length > 2)
                {
                    if (Session["mainlistComunications"] != null)
                    {
                        List<OrderHistory> nList = new List<OrderHistory>();

                        nList = ((List<OrderHistory>)Session["mainlistComunications"]).FindAll(x => x.CustomerNumber.ToLower().Contains(TextBox1.Text.ToLower()));
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistComunications"]).FindAll(x => x.PruductName.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistComunications"]).FindAll(x => x.SellsInitials.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistComunications"]).FindAll(x => x.DateFinalized.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistComunications"]).FindAll(x => x.OrderNumber.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        GridView_Products.DataSource = nList;
                        GridView_Products.DataBind();
                        GridStyle(GridView_Products);
                    }
                }

            }
            catch { Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif"; }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Panel2.Visible = true;
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["mainlistComunications"] != null)
            {
                GridView_Products.DataSource = (List<OrderHistory>)Session["mainlistComunications"];
                GridView_Products.DataBind();
                GridStyle(GridView_Products);
                TextBox1.Text = "";
            }

        }
    }
}