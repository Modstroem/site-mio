﻿using MIO.WebCrmApi1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MIO.Pages
{

    public partial class Supercooler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                Session["LoadingS"] = "15";
                Session["LoadingImageS"] = null;
            }

            if (Session["Supercooler"] != null)
            {
                Session["Supercooler"] = MonitorsFacade.GetInstans().GetTal("Supercooler");
                Label1.Visible = true;
                Label1.Text = Session["antal"].ToString() + " 𝕦𝕕 𝕒𝕗 " + Session["Supercooler"].ToString();
            }



            if (Session["LoadingS2"] == null)
            {

                Random r = new Random();
                int i = r.Next(1, 18);
                Image1.ImageUrl = "~/Images/Gif/" + i + ".gif";
                Session["LoadingS2"] = "Is set";
                Label1.Visible = true;
            }
        }
        public void LoadPage()
        {

            List<OrderHistory> mainlist = new List<OrderHistory>();
            int antal = 0;
            try
            {

                string DGKConnection = "Data Source=SPSQL004;Initial Catalog=DGK_PROD;Persist Security Info=True;User ID=API;password=Duppie36#;";

                SqlConnection conn = new SqlConnection(DGKConnection);
                SqlDataReader reader = null;
                DataTable dataTable = null;

                SqlCommand cmd = new SqlCommand(@"SELECT too.CustomerId As SellsInitials, too.DateFinalized, too.OrderNumber , olp1.[Value] AS CustomerNumber , olp2.[Value] AS CustomerName, olp3.[Value] AS CustomerLastName , ol.[Name] ,too.OrderStatusId , olp4.[Name] , ol.Quantity 
 FROM  [DGK_PROD].[dbo].[TeaCommerce_OrderLine] ol
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_Order]  too on too.Id = ol.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_CustomOrderProperty]  olp1 on too.Id = olp1.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_CustomOrderProperty]  olp2 on too.Id = olp2.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_CustomOrderProperty]  olp3 on too.Id = olp3.OrderId
LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_OrderStatus]  olp4 on olp4.Id = too.OrderStatusId 
 where ol.Sku like '%DKG2S1P5095%' and  olp1.[Alias] = 'customerId' and  olp2.[Alias] = 'firstName' and olp3.[Alias] = 'lastName' and too.OrderStatusId 
 <> 80  and too.OrderStatusId <> 98  and too.OrderStatusId <> 99 and too.OrderStatusId <> 10 and too.OrderStatusId <> 7 and too.OrderStatusId <> 100 and too.DateFinalized is not null order by too.DateFinalized desc
", conn);

                cmd.CommandType = CommandType.Text;
                conn.Open();
                reader = cmd.ExecuteReader();
                dataTable = new DataTable();
                dataTable.Load(reader);
                reader.Close();
                conn.Close();

                int i = 1;
                foreach (DataRow row in dataTable.Rows)
                {
                    try
                    {
                        string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();

                        OrderHistory oh = new OrderHistory();

                        oh.SellsInitials = fields[0];
                        oh.DateFinalized = DateTime.Parse(fields[1]).Date.ToShortDateString().ToString();
                        oh.OrderNumber = fields[2];
                        oh.CustomerNumber = fields[3];
                        oh.CustomerName = fields[4] + " " + fields[5];
                        oh.PruductName = fields[6];
                        oh.OrderStatus = fields[8];
                        int omg = 0;
                        try
                        {
                            string[] t = fields[9].Split(',');
                            omg = Convert.ToInt32(t[0].ToString());
                        }
                        catch
                        {
                            Image1.Visible = true;
                            Image1.ImageUrl = "~/Images/Gif/e.gif";
                        }

                        while (omg != 0)
                        {
                            mainlist.Add(oh);
                            antal++;
                            omg--;
                        }
                    }
                    catch (Exception ex)
                    {
                        Image1.Visible = true;
                        Image1.ImageUrl = "~/Images/Gif/e.gif";
                    }
                }




                if (Session["Supercooler"] == null)
                {
                    Session["Supercooler"] = MonitorsFacade.GetInstans().GetTal("Supercooler");
                }

                Label1.Visible = true;
                Label1.Text = antal.ToString() + " 𝕦𝕕 𝕒𝕗 " + Session["Supercooler"].ToString();

                Session["antal"] = antal.ToString(); ;
            }
            catch
            {
                Image1.Visible = true;
                Image1.ImageUrl = "~/Images/Gif/e.gif";
            }


            //int i = 0;
            //int total = mainlist.Count;
            //foreach (OrderHistory oh in mainlist)
            //{
            //    //lblError.Text = oh.CustomerName;
            //   // WebCrmOrganisation web = GetWebCrmOrganisation(Convert.ToInt32(oh.CustomerNumber));
            //    if (web != null)
            //    {
            //        //string Descripton = Facade.GetInstans().GetStatusDescripton(Convert.ToInt32(oh.OrderStatus));
            //        //oh.OrderStatus += Descripton + " - " + web.GetWebCrmOpportunity(oh.OrderNumber).StatusSL;

            //        //Facade.GetInstans().Test = "(" + i + " / " + total + ") Getting data for : CustomerNumber " + oh.CustomerNumber + " Status " + oh.OrderStatus;

            //    }
            //    i++;
            //}

            //lblInfo.Text = "";
            //Facade.GetInstans().Test = "";

            if (mainlist.Count != 0)
            {
                Session["mainlistS"] = mainlist;
            }

            GridView_Products.DataSource = mainlist;
            GridView_Products.DataBind();
            GridStyle(GridView_Products);

            Session["LoadingS"] = "0";
        }

        int rowNumber = 0;
        int SellsInitials = 1;
        int DateFinalized = 2;
        int OrderNumber = 3;
        int CustomerNumber = 4;
        int CustomerName = 5;
        int PruductName = 6;
        int OrderStatus = 7;


        public int BusinessDaysUntil(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                   1 + ((endD - startD).TotalDays * 5 -
                   (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return (int)calcBusinessDays;
        }

        public void GridStyle(GridView gw)
        {
            try
            {
                gw.HeaderRow.Cells[rowNumber].Text = "Number";
                gw.HeaderRow.Cells[SellsInitials].Text = "Sells Initials";
                gw.HeaderRow.Cells[DateFinalized].Text = "Date";
                gw.HeaderRow.Cells[OrderNumber].Text = "Order Number";
                gw.HeaderRow.Cells[CustomerNumber].Text = "Customer Number";
                gw.HeaderRow.Cells[CustomerName].Text = "Customer Name";
                gw.HeaderRow.Cells[PruductName].Text = "Status";
                gw.HeaderRow.Cells[OrderStatus].Text = "Product";

                gw.HeaderRow.Cells[SellsInitials].Width = 80;
                gw.HeaderRow.Cells[DateFinalized].Width = 80;
                gw.HeaderRow.Cells[OrderNumber].Width = 80;
                gw.HeaderRow.Cells[CustomerNumber].Width = 60;
                gw.HeaderRow.Cells[CustomerName].Width = 100;
                gw.HeaderRow.Cells[PruductName].Width = 50;
                gw.HeaderRow.Cells[OrderStatus].Width = 250;





                for (int i = 0; i <= gw.Rows.Count - 1; i++)
                {


                    gw.Rows[i].Cells[rowNumber].Visible = false;
                    gw.HeaderRow.Cells[rowNumber].Visible = false;

                    gw.HeaderRow.Cells[SellsInitials].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[DateFinalized].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[OrderNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[CustomerNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[CustomerName].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[PruductName].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[OrderStatus].HorizontalAlign = HorizontalAlign.Left;


                    gw.Rows[i].Cells[SellsInitials].Width = 80;
                    gw.Rows[i].Cells[DateFinalized].Width = 80;
                    gw.Rows[i].Cells[OrderNumber].Width = 60;
                    gw.Rows[i].Cells[CustomerNumber].Width = 60;
                    gw.Rows[i].Cells[CustomerName].Width = 100;
                    gw.Rows[i].Cells[PruductName].Width = 50;
                    gw.Rows[i].Cells[OrderStatus].Width = 250;


                    try
                    {
                        if (BusinessDaysUntil(DateTime.Parse(gw.Rows[i].Cells[2].Text), DateTime.Now) >= 20)
                        {
                            //if (gw.Rows[i].Cells[PruductName].Text.Contains("Afventer kundens accept") == true)
                            //{
                            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                            //}
                        }


                        if (gw.Rows[i].Cells[PruductName].Text.Contains("Unable to complete") == true)
                        {
                            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                        }

                    }
                    catch { }


                    gw.Rows[i].Cells[SellsInitials].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[DateFinalized].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[OrderNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[CustomerNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[CustomerName].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[PruductName].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[OrderStatus].HorizontalAlign = HorizontalAlign.Left;


                }
            }
            catch (Exception ex)
            {
                Session["Error"] += ex.Message + Environment.NewLine + ex.StackTrace;
            }
        }

        private DateTime ConvertToDate(string customDate)
        {
            DateTime dt = DateTime.MinValue;


            try
            {
                string date = customDate;

                if (customDate.Contains(".")) { date = customDate.Replace(".", ""); }
                if (customDate.Contains("-")) { date = customDate.Replace("-", ""); }


                string YYYY = date[0].ToString() + date[1].ToString() + date[2].ToString() + date[3].ToString();
                string MM = date[4].ToString() + date[5].ToString();
                string DD = date[6].ToString() + date[7].ToString();
                dt = DateTime.Parse(YYYY + "-" + MM + "-" + DD);
            }
            catch { }

            if (dt == DateTime.MinValue)
            {
                try
                {
                    dt = DateTime.Parse(customDate);
                }
                catch { }
            }

            return dt;
        }



        protected void GridView_Products_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GridView_Products_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public class OrderHistory
        {
            public int rowId { get; set; }
            public string SellsInitials { get; set; }
            public string DateFinalized { get; set; }
            public string OrderNumber { get; set; }
            public string CustomerNumber { get; set; }
            public string CustomerName { get; set; }
            public string OrderStatus { get; set; }
            public string PruductName { get; set; }


            public OrderHistory()
            {

            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (Session["LoadingS"].ToString() == "0")
            {
                Timer1.Enabled = false;
                Label1.Text = "";
                Image1.ImageUrl = "";
                Image1.Visible = false;
                Label1.Visible = false;
                this.GridView_Products.Visible = true;

                if (Session["Supercooler"] != null)
                {
                    Session["Supercooler"] = MonitorsFacade.GetInstans().GetTal("Supercooler");
                    Label1.Visible = true;
                    Label1.Text = Session["antal"].ToString() + " 𝕦𝕕 𝕒𝕗 " + Session["Supercooler"].ToString();
                }
            }
            else
            {
                //int f = Convert.ToInt32(Session["LoadingS"].ToString());
                //f = f - 1;
                //Session["LoadingS"] = f.ToString();
                this.GridView_Products.Visible = false;

                //Label1.Text = "Wait";
                try
                {
                    if (Session["LoadingImageS"] == null)
                    {

                        Random r = new Random();
                        int i = r.Next(1, 18);
                        Image1.ImageUrl = "~/Images/Gif/" + i + ".gif";
                        Session["LoadingImageS"] = "Is set";
                        Label1.Visible = true;
                        LoadPage();
                    }
                }
                catch { Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif"; }

            }




        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Panel2.Visible = false;
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (TextBox1.Text.Length > 2)
                {
                    if (Session["mainlistS"] != null)
                    {
                        List<OrderHistory> nList = new List<OrderHistory>();

                        nList = ((List<OrderHistory>)Session["mainlistS"]).FindAll(x => x.OrderStatus.ToLower().Contains(TextBox1.Text.ToLower()));
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistS"]).FindAll(x => x.PruductName.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistS"]).FindAll(x => x.CustomerName.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistS"]).FindAll(x => x.CustomerNumber.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistS"]).FindAll(x => x.OrderNumber.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        GridView_Products.DataSource = nList;
                        GridView_Products.DataBind();
                        GridStyle(GridView_Products);
                    }
                }

            }
            catch { Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif"; }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Panel2.Visible = true;
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["mainlistS"] != null)
            {
                GridView_Products.DataSource = (List<OrderHistory>)Session["mainlistS"];
                GridView_Products.DataBind();
                GridStyle(GridView_Products);
                TextBox1.Text = "";
            }

        }
    }
}