﻿using MIO.WebCrmApi1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace MIO.Pages
{

    public partial class dgt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                Session["Loadingdgt"] = "15";
                Session["LoadingImagedgt"] = null;
            }

            if (Session["LoadingImagedgt2"] == null)
            {

                Random r = new Random();
                int i = r.Next(1, 18);
                Image1.ImageUrl = "~/Images/Gif/" + i + ".gif";
                Session["LoadingImagedgt2"] = "Is set";
                Label1.Visible = true;
            }
        }
        public void LoadPage()
        {
            //if (Image1.Visible == true)
            //{
            //    Image1.Visible = false;
            //    Image1.ImageUrl = "";
            //}

            List<OrderHistory> mainlist = new List<OrderHistory>();

            try
            {

                string DGKConnection = "Data Source=SPSQL004;Initial Catalog=DGK_PROD;Persist Security Info=True;User ID=API;password=Duppie36#;";

                SqlConnection conn = new SqlConnection(DGKConnection);
                SqlDataReader reader = null;
                DataTable dataTable = null;

                SqlCommand cmd = new SqlCommand(@"
 SELECT TOP (100)  too.CustomerId As SellsInitials, too.DateFinalized, too.OrderNumber , olp1.[Value] AS CustomerNumber , olp2.[Value] AS CustomerName, olp3.[Value] AS CustomerLastName , ol.[Name] ,too.OrderStatusId, olp4.[Name] 
 FROM  [DGK_PROD].[dbo].[TeaCommerce_OrderLine] ol
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_Order]  too on too.Id = ol.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_CustomOrderProperty]  olp1 on too.Id = olp1.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_CustomOrderProperty]  olp2 on too.Id = olp2.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_CustomOrderProperty]  olp3 on too.Id = olp3.OrderId
 LEFT JOIN [DGK_PROD].[dbo].[TeaCommerce_OrderStatus]  olp4 on olp4.Id = too.OrderStatusId 
 where ol.Sku like '%DKG6S1%' and  olp1.[Alias] = 'customerId' and  olp2.[Alias] = 'firstName' and olp3.[Alias] = 'lastName' and too.OrderStatusId 
 <> 80  and too.OrderStatusId <> 98  and too.OrderStatusId <> 99 order by too.DateFinalized desc
", conn);

                cmd.CommandType = CommandType.Text;
                conn.Open();
                reader = cmd.ExecuteReader();
                dataTable = new DataTable();
                dataTable.Load(reader);
                reader.Close();
                conn.Close();

                int i = 1;
                foreach (DataRow row in dataTable.Rows)
                {
                    try
                    {
                        string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();

                        OrderHistory oh = new OrderHistory();
                        oh.SellsInitials = fields[0];
                        oh.DateFinalized = DateTime.Parse(fields[1]).Date.ToShortDateString().ToString();
                        oh.OrderNumber = fields[2];
                        oh.CustomerNumber = fields[3];
                        oh.CustomerName = fields[4] + " " + fields[5];
                        oh.PruductName = fields[6];
                        oh.OrderStatus = fields[8];
                        oh.WebCrmInitials = "?";
                        mainlist.Add(oh);
                    }
                    catch (Exception ex)
                    {
                        Image1.Visible = true;
                        Image1.ImageUrl = "~/Images/Gif/e.gif";
                    }
                }


                foreach (OrderHistory oh in mainlist)
                {
                    WebCrmOrganisation web = GetWebCrmOrganisation(Convert.ToInt32(oh.CustomerNumber));
                    if (web != null)
                    {

                        oh.OrderStatus += " - " + web.GetWebCrmOpportunity(oh.OrderNumber).StatusSL;
                        oh.WebCrmInitials = GetSupervisorIdFromWebCrmViaName(web.GetWebCrmOpportunity(oh.OrderNumber).SupervisorId);
                    }

                }
            }



            catch
            {
                Image1.Visible = true;
                Image1.ImageUrl = "~/Images/Gif/e.gif";
            }



            if (mainlist.Count != 0)
            {
                Session["mainlistdgt"] = mainlist;
            }

            GridView_Products.DataSource = mainlist;
            GridView_Products.DataBind();
            GridStyle(GridView_Products);

            Session["Loadingdgt"] = "0";
        }

        public class WebCrmSupervisor
        {

            public string Name { get; set; }
            public long Supervisor_ID { get; set; }
        }

        public List<WebCrmSupervisor> Supervisors = new List<WebCrmSupervisor>();

        public int InitializeSupervisors()
        {
            int result = 0;
            try
            {
                string WebCrmAccount = "cm25706WMdJad";
                string WebCrmUsername = "modsmods200";
                string WebCrmPassword = "123qweASD!!!";

                List<RetrieveByQueryResultRow> CrmList = new List<RetrieveByQueryResultRow>();
                List<RetrieveByQueryResultRow> CrmListTotal = new List<RetrieveByQueryResultRow>();

                WebCrmApiSoap proxy = null;
                AuthenticateRequest ar = null;
                AuthenticateResponse th = null;

                EndpointAddress endpointAdress = new EndpointAddress("https://webcrmapi5.b2bsys.net/WebCrmApi.asmx");
                BasicHttpBinding binding = new BasicHttpBinding();

                binding.Name = "WebCrmApi1";
                binding.CloseTimeout = TimeSpan.FromMinutes(10);
                binding.OpenTimeout = TimeSpan.FromMinutes(10);
                binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
                binding.SendTimeout = TimeSpan.FromMinutes(10);
                binding.MaxReceivedMessageSize = 2147483647;

                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                proxy = new WebCrmApiSoapClient(binding, endpointAdress);
                ar = new AuthenticateRequest(WebCrmAccount, WebCrmUsername, WebCrmPassword);
                th = proxy.Authenticate(ar);

                List<RetrieveByQueryResultRow> WebCrmList = new List<RetrieveByQueryResultRow>();
                List<RetrieveByQueryResultRow> aPerList = new List<RetrieveByQueryResultRow>();
                if (Supervisors.Count == 0)
                {
                    #region Supervisors
                    ReturnAllUsersRequest rbq11 = new ReturnAllUsersRequest(th.TicketHeader);
                    ReturnAllUsersResponse result1 = proxy.ReturnAllUsers(new ReturnAllUsersRequest(th.TicketHeader));
                    var data = result1.ReturnAllUsersResult.Users;
                    if (data != null)
                    {
                        if (data.Count() != 0)
                        {
                            foreach (UserData user in data)
                            {
                                string Name = user.Name.ToLower().Replace(" ", "");

                                WebCrmSupervisor supervisor = new WebCrmSupervisor();
                                supervisor.Name = user.Name.ToLower().Replace(" ", "");
                                supervisor.Supervisor_ID = user.ID;

                                Supervisors.Add(supervisor);

                            }
                        }
                    }

                    if (Supervisors.Count == 0)
                    {
                        return -1;
                    }
                    #endregion
                }


                result = 1;
            }
            catch (Exception ex)
            {
                return -1;
            }

            return result;
        }

        public string GetSupervisorIdFromWebCrmViaName(int SupervisorID)
        {
            string result = "?";

            try
            {
                int result1 = 0;
                if (Supervisors.Count == 0)
                {
                    result1 = InitializeSupervisors();
                }

                if (result1 == -1)
                {
                    return result;
                }

                if (Supervisors != null && Supervisors.Count != 0 && SupervisorID != 0)
                {


                    //if (SupervisorID == "rasmusschultzmadsen")
                    //{
                    //    SupervisorID = "nicolaigrejsengregersen";
                    //}

                    //var s = Supervisors.Find(x => x.Name == SupervisorName);
                    var s = Supervisors.Find(x => x.Supervisor_ID == SupervisorID);
                    if (s != null)
                    {
                        result = s.Name;
                    }
                    else
                    {
                        s = Supervisors.Find(x => x.Name == "api");
                    }
                }
                else
                {
                    result = "-1";
                }
            }
            catch
            {
                result = "-1";
            }

            return result;
        }

        int rowNumber = 0;
        int SellsInitials = 1;
        int DateFinalized = 2;
        int OrderNumber = 3;
        int CustomerNumber = 4;
        int CustomerName = 5;
        int PruductName = 6;
        int OrderStatus = 7;
        int SellsInitials2 = 8;


        public int BusinessDaysUntil(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                   1 + ((endD - startD).TotalDays * 5 -
                   (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return (int)calcBusinessDays;
        }

        public WebCrmOrganisation GetWebCrmOrganisation(int lbnr)
        {
            WebCrmOrganisation org = new WebCrmOrganisation();
            org.Id = 0;
            org.Error = null;
            try
            {
                string WebCrmAccount = "cm25706WMdJad";
                string WebCrmUsername = "modsmods200";
                string WebCrmPassword = "123qweASD!!!";

                List<RetrieveByQueryResultRow> CrmList = new List<RetrieveByQueryResultRow>();
                List<RetrieveByQueryResultRow> CrmListTotal = new List<RetrieveByQueryResultRow>();

                WebCrmApiSoap proxy = null;
                AuthenticateRequest ar = null;
                AuthenticateResponse th = null;

                EndpointAddress endpointAdress = new EndpointAddress("https://webcrmapi5.b2bsys.net/WebCrmApi.asmx");
                BasicHttpBinding binding = new BasicHttpBinding();

                binding.Name = "WebCrmApi1";
                binding.CloseTimeout = TimeSpan.FromMinutes(10);
                binding.OpenTimeout = TimeSpan.FromMinutes(10);
                binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
                binding.SendTimeout = TimeSpan.FromMinutes(10);
                binding.MaxReceivedMessageSize = 2147483647;

                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                proxy = new WebCrmApiSoapClient(binding, endpointAdress);
                ar = new AuthenticateRequest(WebCrmAccount, WebCrmUsername, WebCrmPassword);
                th = proxy.Authenticate(ar);


                string select = "";



                List<RetrieveByQueryResultRow> WebCrmList = new List<RetrieveByQueryResultRow>();
                List<RetrieveByQueryResultRow> aPerList = new List<RetrieveByQueryResultRow>();

                #region Organisations
                select = "";
                int organisation_ID = 0;
                int o_compareName = 1;
                int o_Ucustom3 = 2;
                int o_telephone = 3;



                //select = "organisation_ID, o_owner , o_owner2 , o_created, o_createdby, o_lastDisplayed, o_lastUpdate, o_lastUpdateBy, o_territory, o_organisation, o_divisionName, o_compareName, o_www, o_telephone, o_fax, o_adr, o_postcode, o_city, o_country, o_type, o_status, o_industry, o_alert, o_custom1, o_custom2, o_custom3, o_custom4, o_custom5, o_custom6, o_custom7, o_custom8, o_comment, o_Ucustom1, o_Ucustom2, o_Ucustom3, o_Ucustom4, o_Ucustom5, o_Ucustom6, o_Ucustom7, o_Ucustom8, o_imageUploaded, o_outlookSync, o_state, o_teleSearch, o_memo, o_custom9, o_custom10, o_custom11, o_custom12, o_custom13, o_custom14, o_custom15, o_gps, o_overlayURL, o_lastItemUpdate, o_lastItemType, o_note, o_vatGroup, o_marketDataId, o_domain, o_noAds, o_approvalStatus, o_xInt1, o_xInt2, o_xInt3, o_xInt4, o_xInt5, o_xInt6, o_xInt7, o_xInt8, o_xDate1, o_xDate2, o_xText1, o_xText2, o_xText3, o_xText4, o_xText5, o_xText6, o_xText7, o_xText8, o_xMemo1, o_xMemo2, o_sla, o_reportTemp, o_vatCountry, o_vatNr, o_vatVerified, o_vatStatus";
                select = "organisation_ID, o_compareName, o_Ucustom3 , o_telephone";

                RetrieveByQueryRequest rbq = new RetrieveByQueryRequest(th.TicketHeader, select, "organisation", "((1 = 1)) and o_Ucustom3 = '" + lbnr + "'", "organisation_ID");
                RetrieveByQueryResponse retrieveByQueryResponse = proxy.RetrieveByQuery(rbq);

                if (retrieveByQueryResponse != null)
                {
                    if (retrieveByQueryResponse.RetrieveByQueryResult != null)
                    {
                        if (retrieveByQueryResponse.RetrieveByQueryResult.Rows != null)
                        {
                            if (retrieveByQueryResponse.RetrieveByQueryResult.Rows.Length != 0)
                            {
                                CrmList.Clear();
                                CrmList = new List<RetrieveByQueryResultRow>(retrieveByQueryResponse.RetrieveByQueryResult.Rows);
                                CrmListTotal.AddRange(CrmList);

                            }
                        }
                    }
                }


                if (CrmListTotal.Count == 1)
                {
                    try
                    {
                        org = new WebCrmOrganisation();
                        foreach (RetrieveByQueryResultRow r in CrmListTotal.AsEnumerable<RetrieveByQueryResultRow>())
                        {
                            try
                            {
                                org.Id = Convert.ToInt32(r.FieldValues[organisation_ID].ToString());
                                org.Name = "?";
                                if (r.FieldValues[o_compareName].ToString() != null && r.FieldValues[o_compareName].ToString() != "") { org.Name = r.FieldValues[o_compareName].ToString(); }
                                try { org.Lbnr = Convert.ToInt32(r.FieldValues[o_Ucustom3].ToString()); } catch { org.Lbnr = -1; }
                                org.Phone = "?";
                                if (r.FieldValues[o_telephone].ToString() != null && r.FieldValues[o_telephone].ToString() != "") { org.Phone = r.FieldValues[o_telephone].ToString(); }
                            }
                            catch (Exception ex)
                            {
                                org.Id = -1;
                                org.Lbnr = -1;
                                org.Error += ex.Message + " " + ex.StackTrace;
                            }

                        }

                        CrmList.Clear();
                        CrmListTotal.Clear();
                    }
                    catch (Exception ex)
                    {
                        org.Error += ex.Message + " " + ex.StackTrace;
                    }
                }
                else
                {
                    //org.Id = -1;
                    //org.Lbnr = -1;
                    //org.Error += "Not found";
                }
                #endregion

                #region Opportunitys
                int op_orgId = 0;
                int op_number = 1;
                int op_levelText = 2;
                //int op_orderDate = 3;
                int op_custom3 = 4;
                int op_orderDateFinal = 5;
                int opportunity_ID = 6;
                int OpportunityExpected = 7;
                int OpportunityDelivery = 8;
                int opc_custom7 = 39;
                int op_description = 9;
                int op_assignedTo = 12;
                // int OpportunityPlanned = 8;
                //string bizarreCatcher = "";
                int op_history = 25;

                if (org.Id != 0 && org.Id != -1)
                {
                    //All op_levelText
                    select = "op_orgId,op_number,op_levelText,op_orderDate,op_custom3, opc_custom2, opportunity_ID ,opc_custom3 ,opc_custom4 ,op_description ,op_personId ,op_assignedTo ,op_assignedTo2 ,op_userGroup ,op_product ,op_orderValue ,op_orderGMvalue ,op_discount ,op_currency ,op_level ,op_percent ,op_nextFollowUp ,op_lost1 ,op_lost2 ,op_lost3 ,op_history ,op_custom1 ,op_custom2 ,op_custom4 ,op_custom5 ,op_custom6 ,op_custom7 ,op_custom8 ,op_readOnly ,op_readyForErp ,op_erpSyncTime ,op_erpId ,op_created ,op_createdBy ,op_lastUpdate ,op_lastUpdateBy ,opc_description ,opc_personId ,opc_deliveryResponsible ,opc_currency ,opc_number ,opc_risk ,opc_status ,op_history ,op_duration ,opc_custom1 ,opc_custom5 ,opc_custom6 ,opc_custom7 ,opc_custom8 ,opc_custom9 ,opc_custom10 ,opc_custom11 ,opc_custom12 ,opc_custom13 ,opc_custom14 ,opc_custom15 ";

                    //select = "op_orgId,op_number,op_levelText,op_orderDate,op_custom3,opc_custom2,opportunity_ID,opc_custom3,opc_custom4,op_assignedTo";


                    rbq = new RetrieveByQueryRequest(th.TicketHeader, select, "Opportunity", "((1 = 1)) and op_orgId = " + org.Id, "op_orgId");
                    retrieveByQueryResponse = proxy.RetrieveByQuery(rbq);

                    if (retrieveByQueryResponse != null)
                    {
                        if (retrieveByQueryResponse.RetrieveByQueryResult != null)
                        {
                            if (retrieveByQueryResponse.RetrieveByQueryResult.Rows != null)
                            {
                                if (retrieveByQueryResponse.RetrieveByQueryResult.Rows.Length != 0)
                                {
                                    CrmList.Clear();
                                    CrmList = new List<RetrieveByQueryResultRow>(retrieveByQueryResponse.RetrieveByQueryResult.Rows);
                                    CrmListTotal.AddRange(CrmList);

                                }
                            }
                        }
                    }
                }

                if (CrmListTotal.Count != 0)
                {
                    try
                    {

                        foreach (RetrieveByQueryResultRow r in CrmListTotal.AsEnumerable<RetrieveByQueryResultRow>())
                        {
                            WebCrmOpportunity opp = new WebCrmOpportunity();
                            try
                            {

                                opp.SupervisorId = Convert.ToInt32(r.FieldValues[op_assignedTo].ToString());
                                opp.OrgId = Convert.ToInt32(r.FieldValues[op_orgId].ToString());
                                opp.Number = r.FieldValues[op_number].ToString();
                                opp.PipelineNiveau = r.FieldValues[op_levelText].ToString();
                                opp.StatusSL = r.FieldValues[op_custom3].ToString();
                                try
                                {
                                    opp.opportunity_ID = Convert.ToInt64(r.FieldValues[opportunity_ID].ToString());
                                }
                                catch (Exception ex)
                                {
                                    org.Error += ex.Message + " " + ex.StackTrace;
                                }
                                opp.ProductName = r.FieldValues[op_description].ToString();

                                //Forventet leveringsdato: 	    opc_custom3 = 7
                                //Forventet opstartdato:		opc_custom4 8
                                //Salgsdato:                    opc_custom14 60 - 2018.08.20
                                //1.betaling modtaget           opc_custom15 61 = 2018.06.26
                                //Slutfaktureringsdato:         opc_custom2 5
                                //Førstegangsfaktureringsdato:  opc_custom1  50 = 2018.06.25
                                //Dato for møde                 op_custom6   30 = 2018.06.25
                                //              opc_custom7

                                //opp.OpportunityFinalBillingDate = ConvertToDate(r.FieldValues[op_orderDateFinal].ToString());

                                opp.OpportunityFinalBillingDate = ConvertToDate(r.FieldValues[opc_custom7].ToString());
                                opp.OpportunityExpectedStartDate = ConvertToDate(r.FieldValues[OpportunityExpected].ToString());
                                // opp.OpportunityExpectedDeliveryDate = ConvertToDate(r.FieldValues[OpportunityDelivery].ToString());

                                // opp.OpportunityPlanned = ConvertToDate(r.FieldValues[OpportunityPlanned].ToString());

                                try
                                {
                                    opp.OpportunityOps = new List<string>();
                                    opp.OpportunityOps.Add("op_history=" + r.FieldValues[op_history].ToString());
                                }
                                catch
                                {
                                    opp.OpportunityOps = new List<string>();
                                    opp.OpportunityOps.Add("Error setting OpportunityOps");
                                }

                                try
                                {
                                    string[] rt = select.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                    int i = 0;
                                    foreach (string s in rt)
                                    {
                                        opp.OpportunityLongString += i + "=" + s + "=" + r.FieldValues[i] + Environment.NewLine;
                                        i++;
                                    }

                                    if (opp.Number.Contains("V0015158"))
                                    {
                                        var d = opp.OpportunityLongString;
                                    }
                                }
                                catch { }




                            }
                            catch (Exception ex)
                            {
                                org.Id = -1;
                                org.Error += ex.Message + " " + ex.StackTrace;
                            }
                            org.Opportunitys.Add(opp);
                        }

                        CrmList.Clear();
                        CrmListTotal.Clear();
                    }
                    catch (Exception ex)
                    {
                        org.Id = -1;
                        org.Error += ex.Message + " " + ex.StackTrace;

                    }
                }
                else
                {

                }
                #endregion

                #region Persons

                if (org.Id != 0 && org.Id != -1)
                {
                    select = "";

                    int p_organisationID = 0;
                    int person_ID = 1;
                    int p_email = 2;
                    int p_firstName = 3;
                    int p_lastName = 4;
                    int p_name = 5;
                    int p_status = 6;



                    select = "p_organisationID,person_ID , p_email ,p_firstName ,p_lastName ,p_name ,p_status";
                    rbq = new RetrieveByQueryRequest(th.TicketHeader, select, "Person", "((1 = 1)) and p_organisationID  = " + org.Id, "person_ID");
                    retrieveByQueryResponse = proxy.RetrieveByQuery(rbq);

                    if (retrieveByQueryResponse != null)
                    {
                        if (retrieveByQueryResponse.RetrieveByQueryResult != null)
                        {
                            if (retrieveByQueryResponse.RetrieveByQueryResult.Rows != null)
                            {
                                if (retrieveByQueryResponse.RetrieveByQueryResult.Rows.Length != 0)
                                {
                                    CrmList.Clear();
                                    CrmList = new List<RetrieveByQueryResultRow>(retrieveByQueryResponse.RetrieveByQueryResult.Rows);
                                    CrmListTotal.AddRange(CrmList);

                                }
                            }
                        }
                    }

                    if (CrmListTotal.Count != 0)
                    {
                        try
                        {
                            foreach (RetrieveByQueryResultRow r in CrmListTotal.AsEnumerable<RetrieveByQueryResultRow>())
                            {
                                WebCrmPerson per = new WebCrmPerson();
                                try
                                {
                                    per.OrgId = Convert.ToInt32(r.FieldValues[p_organisationID].ToString());
                                    per.Id = Convert.ToInt32(r.FieldValues[person_ID].ToString());
                                    per.FirstName = r.FieldValues[p_firstName].ToString();
                                    per.LastName = r.FieldValues[p_lastName].ToString();
                                    per.Name = r.FieldValues[p_name].ToString();
                                    per.Status = r.FieldValues[p_status].ToString();
                                    per.Email = "?";
                                    if (r.FieldValues[p_email] != null && r.FieldValues[p_email].ToString() != "" && r.FieldValues[p_email].ToString().Contains("@")) { per.Email = r.FieldValues[p_email].ToString().ToLower(); }
                                }
                                catch (Exception ex)
                                {
                                    per.Id = -1;
                                    per.OrgId = -1;
                                    org.Error += ex.Message + " " + ex.StackTrace;
                                }
                                org.WebCrmPersons.Add(per);
                            }

                            CrmList.Clear();
                            CrmListTotal.Clear();
                        }
                        catch (Exception ex)
                        {
                            org.Error += ex.Message + " " + ex.StackTrace;
                        }
                    }
                    else
                    {

                    }
                }
                #endregion

                #region Activities / TO-DO
                int activity_ID = 0;
                int a_date = 1;
                int a_assignedTo = 2;
                int a_personId = 4;
                int a_action = 5;
                int a_description = 6;
                int a_product = 7;
                int a_history = 8;
                int a_autoComplete = 9;
                int a_custom1 = 10;
                int a_outlookSync = 11;
                int a_created = 12;
                int a_createdBy = 13;
                int a_organisationID = 16;
                int a_opportunityId = 17;
                int a_campaignId = 18;

                if (org.Id != 0 && org.Id != -1)
                {
                    //All op_levelText
                    select = "activity_ID ,a_date ,a_assignedTo ,a_personId ,a_action ,a_status ,a_description ,a_product ,a_history ,a_autoComplete ,a_custom1 ,a_outlookSync ,a_created ,a_createdBy ,a_lastUpdate ,a_lastUpdateBy ,a_organisationID ,a_opportunityId ,a_campaignId";

                    //activity_ID ,a_date ,a_assignedTo ,a_personId ,a_action ,a_status ,a_description ,a_product ,a_history ,a_autoComplete ,a_custom1 
                    //,a_outlookSync ,a_created ,a_createdBy ,a_lastUpdate ,a_lastUpdateBy ,a_organisationID ,a_opportunityId ,a_campaignId

                    //select = "op_orgId,op_number,op_levelText,op_orderDate,op_custom3,opc_custom2,opportunity_ID,opc_custom3,opc_custom4";


                    rbq = new RetrieveByQueryRequest(th.TicketHeader, select, "Activity", "((1 = 1)) and a_organisationID = " + org.Id, "a_organisationID");
                    retrieveByQueryResponse = proxy.RetrieveByQuery(rbq);

                    if (retrieveByQueryResponse != null)
                    {
                        if (retrieveByQueryResponse.RetrieveByQueryResult != null)
                        {
                            if (retrieveByQueryResponse.RetrieveByQueryResult.Rows != null)
                            {
                                if (retrieveByQueryResponse.RetrieveByQueryResult.Rows.Length != 0)
                                {
                                    CrmList.Clear();
                                    CrmList = new List<RetrieveByQueryResultRow>(retrieveByQueryResponse.RetrieveByQueryResult.Rows);
                                    CrmListTotal.AddRange(CrmList);

                                }
                            }
                        }
                    }
                }

                if (CrmListTotal.Count != 0)
                {
                    try
                    {

                        foreach (RetrieveByQueryResultRow r in CrmListTotal.AsEnumerable<RetrieveByQueryResultRow>())
                        {
                            WebCrmActivity opp = new WebCrmActivity();
                            try
                            {


                                //int activity_ID = 0;
                                //int a_date = 1;
                                //int a_assignedTo = 2;
                                //int a_personId = 4;
                                //int a_action = 5;
                                //int a_description = 6;
                                //int a_product = 7;
                                //int a_history = 8;
                                //int a_autoComplete = 9;
                                //int a_custom1 = 10;
                                //int a_outlookSync = 11;
                                //int a_created = 12;
                                //int a_createdBy = 13;
                                //int a_organisationID = 16;
                                //int a_opportunityId = 17;
                                //int a_campaignId = 18;


                                opp.Id = Convert.ToInt64(r.FieldValues[activity_ID].ToString());
                                opp.FolowUpDate = ConvertToDate(r.FieldValues[a_date].ToString());
                                opp.SupervisorId = r.FieldValues[a_assignedTo].ToString();
                                opp.PersonId = r.FieldValues[a_personId].ToString();
                                opp.Action = r.FieldValues[a_action].ToString();
                                opp.Description = r.FieldValues[a_description].ToString();
                                opp.Product = r.FieldValues[a_product].ToString();
                                opp.History = r.FieldValues[a_history].ToString();
                                opp.OpportunityId = Convert.ToInt64(r.FieldValues[a_opportunityId].ToString());

                                if (org.GetWebCrmOpportunity(opp.OpportunityId) != null)
                                {
                                    string Number =
                                    opp.Number = org.GetWebCrmOpportunity(opp.OpportunityId).Number;
                                }
                                else
                                {
                                    opp.Number = "0";
                                }

                                try
                                {
                                    string[] rt = select.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                    int i = 0;
                                    string OpportunityLongString = "";
                                    foreach (string s in rt)
                                    {
                                        OpportunityLongString += i + "=" + s + "=" + r.FieldValues[i] + Environment.NewLine;
                                        i++;
                                    }

                                    if (opp.Number.Contains("V0015158"))
                                    {
                                        // var d = opp.OpportunityLongString;
                                    }
                                }
                                catch { }




                            }
                            catch (Exception ex)
                            {
                                org.Id = -1;
                                org.Error += ex.Message + " " + ex.StackTrace;
                            }
                            org.Activitys.Add(opp);
                        }

                        CrmList.Clear();
                        CrmListTotal.Clear();
                    }
                    catch (Exception ex)
                    {
                        org.Id = -1;
                        org.Error += ex.Message + " " + ex.StackTrace;

                    }
                }
                else
                {

                }
                #endregion


            }
            catch (Exception ex)
            {
                org.Id = -1;
                org.Error += ex.Message + " " + ex.StackTrace;
            }

            return org;
        }

        public void GridStyle(GridView gw)
        {
            try
            {
                gw.HeaderRow.Cells[rowNumber].Text = "Number";
                gw.HeaderRow.Cells[SellsInitials].Text = "Sells Initials";
                gw.HeaderRow.Cells[DateFinalized].Text = "Date";
                gw.HeaderRow.Cells[OrderNumber].Text = "Order Number";
                gw.HeaderRow.Cells[CustomerNumber].Text = "Customer Number";
                gw.HeaderRow.Cells[CustomerName].Text = "Customer Name";
                gw.HeaderRow.Cells[PruductName].Text = "Status";
                gw.HeaderRow.Cells[OrderStatus].Text = "Product";

                gw.HeaderRow.Cells[SellsInitials].Width = 80;
                gw.HeaderRow.Cells[DateFinalized].Width = 120;
                gw.HeaderRow.Cells[OrderNumber].Width = 80;
                gw.HeaderRow.Cells[CustomerNumber].Width = 60;
                gw.HeaderRow.Cells[CustomerName].Width = 100;
                gw.HeaderRow.Cells[PruductName].Width = 250;
                gw.HeaderRow.Cells[OrderStatus].Width = 250;

                for (int i = 0; i <= gw.Rows.Count - 1; i++)
                {


                    gw.Rows[i].Cells[rowNumber].Visible = false;
                    gw.HeaderRow.Cells[rowNumber].Visible = false;

                    gw.HeaderRow.Cells[SellsInitials].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[DateFinalized].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[OrderNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[CustomerNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[CustomerName].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[PruductName].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[OrderStatus].HorizontalAlign = HorizontalAlign.Left;


                    gw.Rows[i].Cells[SellsInitials].Width = 80;
                    gw.Rows[i].Cells[DateFinalized].Width = 120;
                    gw.Rows[i].Cells[OrderNumber].Width = 60;
                    gw.Rows[i].Cells[CustomerNumber].Width = 60;
                    gw.Rows[i].Cells[CustomerName].Width = 100;
                    gw.Rows[i].Cells[PruductName].Width = 250;
                    gw.Rows[i].Cells[OrderStatus].Width = 250;


                    try
                    {
                        if (BusinessDaysUntil(DateTime.Parse(gw.Rows[i].Cells[2].Text), DateTime.Now) >= 20)
                        {
                            if (gw.Rows[i].Cells[PruductName].Text.Contains("Afventer kundens accept") == true)
                            {
                                gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                                gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                            }
                        }


                        if (gw.Rows[i].Cells[PruductName].Text.Contains("Unable to complete") == true)
                        {
                            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                        }

                    }
                    catch { }


                    gw.Rows[i].Cells[SellsInitials].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[DateFinalized].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[OrderNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[CustomerNumber].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[CustomerName].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[PruductName].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[OrderStatus].HorizontalAlign = HorizontalAlign.Left;


                }
            }
            catch (Exception ex)
            {
                Session["Error"] += ex.Message + Environment.NewLine + ex.StackTrace;
            }
        }


        private DateTime ConvertToDate(string customDate)
        {
            DateTime dt = DateTime.MinValue;


            try
            {
                string date = customDate;

                if (customDate.Contains(".")) { date = customDate.Replace(".", ""); }
                if (customDate.Contains("-")) { date = customDate.Replace("-", ""); }


                string YYYY = date[0].ToString() + date[1].ToString() + date[2].ToString() + date[3].ToString();
                string MM = date[4].ToString() + date[5].ToString();
                string DD = date[6].ToString() + date[7].ToString();
                dt = DateTime.Parse(YYYY + "-" + MM + "-" + DD);
            }
            catch { }

            if (dt == DateTime.MinValue)
            {
                try
                {
                    dt = DateTime.Parse(customDate);
                }
                catch { }
            }

            return dt;
        }



        protected void GridView_Products_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GridView_Products_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public class OrderHistory
        {
            public int rowId { get; set; }
            public string SellsInitials { get; set; }
            public string DateFinalized { get; set; }
            public string OrderNumber { get; set; }
            public string CustomerNumber { get; set; }
            public string CustomerName { get; set; }
            public string OrderStatus { get; set; }
            public string PruductName { get; set; }
            public string WebCrmInitials { get; set; }




            public OrderHistory()
            {

            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (Session["Loadingdgt"].ToString() == "0")
            {
                Timer1.Enabled = false;
                Label1.Text = "";
                Image1.ImageUrl = "";
                Image1.Visible = false;
                Label1.Visible = false;
                this.GridView_Products.Visible = true;

            }
            else
            {
                //int f = Convert.ToInt32(Session["Loadingdgt"].ToString());
                //f = f - 1;
                //Session["Loadingdgt"] = f.ToString();
                this.GridView_Products.Visible = false;

                //Label1.Text = "Wait";
                try
                {
                    if (Session["LoadingImagedgt"] == null)
                    {

                        Random r = new Random();
                        int i = r.Next(1, 18);
                        Image1.ImageUrl = "~/Images/Gif/" + i + ".gif";
                        Session["LoadingImagedgt"] = "Is set";
                        Label1.Visible = true;
                        LoadPage();
                    }
                }
                catch { Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif"; }

            }




        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Panel2.Visible = false;
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (TextBox1.Text.Length > 2)
                {
                    if (Session["mainlistdgt"] != null)
                    {
                        List<OrderHistory> nList = new List<OrderHistory>();

                        nList = ((List<OrderHistory>)Session["mainlistdgt"]).FindAll(x => x.OrderStatus.ToLower().Contains(TextBox1.Text.ToLower()));
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistdgt"]).FindAll(x => x.PruductName.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistdgt"]).FindAll(x => x.CustomerName.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistdgt"]).FindAll(x => x.CustomerNumber.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<OrderHistory>)Session["mainlistdgt"]).FindAll(x => x.OrderNumber.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        GridView_Products.DataSource = nList;
                        GridView_Products.DataBind();
                        GridStyle(GridView_Products);
                    }
                }

            }
            catch { Image1.Visible = true; Image1.ImageUrl = "~/Images/Gif/e.gif"; }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Panel2.Visible = true;
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["mainlistdgt"] != null)
            {
                GridView_Products.DataSource = (List<OrderHistory>)Session["mainlistdgt"];
                GridView_Products.DataBind();
                GridStyle(GridView_Products);
                TextBox1.Text = "";
            }

        }

        public class WebCrmActivity
        {
            public long Id { get; set; }
            public DateTime FolowUpDate { get; set; }
            public string Number { get; set; }
            public string SupervisorId { get; set; }
            public string DepartmentId { get; set; }
            public string Action { get; set; }
            public string Description { get; set; }
            public string Status { get; set; }
            public string History { get; set; }
            public string Product { get; set; }
            public string PersonId { get; set; }
            public long OpportunityId { get; set; }

            public WebCrmActivity()
            {

            }
        }
        public class WebCrmOpportunity
        {
            public long opportunity_ID { get; set; }
            public int OrgId { get; set; }
            public string Number { get; set; }
            public string PipelineNiveau { get; set; }
            public string StatusSL { get; set; }
            public DateTime OpportunityFinalBillingDate { get; set; }
            public DateTime OpportunityExpectedStartDate { get; set; }
            public DateTime OpportunityExpectedDeliveryDate { get; set; }
            public DateTime OpportunityPlanned { get; set; }

            public string ProductName { get; set; }
            public int SupervisorId { get; set; }
            public int SalesManager { get; set; }


            public string OpportunityLongString { get; set; }
            public List<string> OpportunityOps { get; set; }



            public WebCrmOpportunity()
            {
                OpportunityFinalBillingDate = DateTime.MinValue;
            }
        }
        public class WebCrmPerson
        {
            public long Id { get; set; }
            public long OrgId { get; set; }
            public int BoLbnr { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Name { get; set; }
            public string Status { get; set; }


            public WebCrmPerson()
            {

            }
        }
        public class WebCrmOrganisation
        {
            public long Id { get; set; }
            public int Lbnr { get; set; }
            public string Name { get; set; }
            public string Phone { get; set; }

            public string Error { get; set; }

            public string KWhforbrug { get; set; }

            public List<WebCrmPerson> WebCrmPersons;
            public List<WebCrmOpportunity> Opportunitys;
            public List<WebCrmActivity> Activitys;


            public WebCrmOrganisation()
            {
                WebCrmPersons = new List<WebCrmPerson>();
                Opportunitys = new List<WebCrmOpportunity>();
                Activitys = new List<WebCrmActivity>();
            }

            public WebCrmOpportunity GetWebCrmOpportunity(string orderNumber)
            {
                WebCrmOpportunity webCrmOpportunity = null;
                try
                {
                    webCrmOpportunity = Opportunitys.Find(x => x.Number.Contains(orderNumber));

                    if (webCrmOpportunity == null)
                    {
                        webCrmOpportunity = new WebCrmOpportunity();
                        webCrmOpportunity.StatusSL = " ? DOES NOT EXIST";
                    }
                }
                catch { }

                return webCrmOpportunity;
            }

            public WebCrmOpportunity GetWebCrmOpportunity(long opportunityId)
            {
                WebCrmOpportunity webCrmOpportunity = null;
                try
                {
                    webCrmOpportunity = Opportunitys.Find(x => x.opportunity_ID == opportunityId);
                }
                catch { }

                return webCrmOpportunity;
            }

            public WebCrmActivity GetWebCrmActivity(string orderNumber)
            {
                WebCrmActivity webCrmActivity = null;
                try
                {
                    webCrmActivity = Activitys.Find(x => x.Number.Contains(orderNumber));
                }
                catch { }

                return webCrmActivity;
            }
        }
    }
}