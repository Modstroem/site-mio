﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ComunicationsLog.aspx.cs" Inherits="MIO.Pages.ComunicationsLog" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .mGrid {
            border-collapse: separate;
            border-spacing: 0;
            background-color: #f8f8f8; /*margin: 5px 9px 10px;*/
            padding-left: 20px;
            padding-right: 20px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

            .mGrid td, .mGrid th {
                padding: 8px 5px;
                color: #717171;
                border-top: 1px solid #fff;
                border-bottom: 1px solid #bbb;
            }

            .mGrid tr:hover {
                background: #d3d3d3 url(images/mgrid-hover.png) repeat-x;
            }

            .mGrid tr:hover {
                background: #d3d3d3 url(images/mgrid-hover.png) repeat-x;
            }

            .mGrid th {
                font-weight: 700;
                border-top: none;
                background: #f8f8f8;
            }

            .mGrid .alt {
            }

            .mGrid .pgr {
            }

                .mGrid .pgr table {
                }

                .mGrid .pgr td {
                    font-weight: 700;
                    color: #ccc;
                    border-bottom: none;
                    background: #f8f8f8;
                }

                    .mGrid .pgr td td {
                        padding: 5px 10px 0 0;
                        border-top: none;
                    }

                .mGrid .pgr a {
                    color: #666;
                    text-decoration: none;
                }

        .auto-style1 {
            width: 303px;
        }

        .auto-style2 {
            position: relative;
            top: 8px;
            left: 0px;
        }
    </style>
    <table style="width: 100%;">
        <tr>
            <td class="text-center"></td>
            <td class="text-center">
                <asp:Label ID="Label2" runat="server" Font-Size="Large" Text="𝕂𝕠𝕞𝕞𝕦𝕟𝕚𝕜𝕒𝕥𝕚𝕠𝕟𝕤𝕝𝕠𝕘" Style="position: relative; top: 10px"></asp:Label></td>
            <td class="text-left">
                &nbsp;</td>
        </tr>
    </table>
    <div class="jumbotron">
        <asp:Panel ID="Panel2" runat="server" Visible="true">
            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBox1" runat="server" OnClientClick="return false;" Width="250px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" CssClass="auto-style2" ImageUrl="~/Images/iconfinder_system-search_118797.png" OnClick="ImageButton3_Click" Width="26px" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ImageButton4" runat="server" CssClass="auto-style2" ImageUrl="~/Images/back.png" Width="26px" OnClick="ImageButton4_Click" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
                    
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                </asp:Timer>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Image ID="Image1" runat="server" BackColor="#EEEEEE" />
                    <asp:Label ID="Label1" runat="server" Text="𝕃𝕠𝕒𝕕𝕚𝕟𝕘" Font-Size="Large" Visible="False"></asp:Label>

                    <br />
                    <asp:GridView ID="GridView_Products" runat="server" CssClass="mGrid" OnSelectedIndexChanged="GridView_Products_SelectedIndexChanged" OnRowDataBound="GridView_Products_RowDataBound" Width="100%">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"
                            HorizontalAlign="Center" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                </asp:Panel>

            </ContentTemplate>

        </asp:UpdatePanel>

                
    </div>
</asp:Content>
