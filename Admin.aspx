﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="MIO.Admin" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <table style="width: 100%;">
            <tr>
                <td style="height: 22px;" colspan="5">
                    <asp:Label ID="Label5" runat="server" Text="Dont touch me, if this module is unknown to you sorry ass." CssClass="btn" Font-Bold="True" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 106px; height: 22px;">
                    &nbsp;</td>
                <td style="width: 115px; height: 22px;">
                    &nbsp;</td>
                <td class="text-center" style="height: 22px; width: 98px">
                    &nbsp;</td>
                <td style="height: 22px; width: 103px;" class="text-center">
                    &nbsp;</td>
                <td style="height: 22px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 106px; height: 22px;">
                    <asp:Label ID="Label1" runat="server" Text="Supercooler" CssClass="btn"></asp:Label>
                </td>
                <td style="width: 115px; height: 22px;">
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="btn"></asp:TextBox>
                </td>
                <td class="text-center" style="height: 22px; width: 98px">
                    <asp:Button ID="button1" runat="server" Text="GET" CssClass="btn-success" OnClick="button1_Click" />
                </td>
                <td style="height: 22px; width: 103px;" class="text-center">
                    <asp:Button ID="button3" runat="server" Text="SET" CssClass="dropdown-toggle btn-danger" OnClick="button3_Click" />
                </td>
                <td style="height: 22px">
                    <asp:Label ID="Label3" runat="server" CssClass="btn"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 19px; width: 106px"></td>
                <td style="height: 19px; width: 115px"></td>
                <td style="height: 19px; width: 98px;" class="text-center"></td>
                <td style="height: 19px; width: 103px;" class="text-center"></td>
                <td style="height: 19px"></td>
            </tr>
            <tr>
                <td style="width: 106px">
                    <asp:Label ID="Label2" runat="server" Text="Gulvventilator" CssClass="btn"></asp:Label>
                </td>
                <td style="width: 115px">
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="btn"></asp:TextBox>
                </td>
                <td class="text-center" style="width: 98px">
                    <asp:Button ID="button2" runat="server" Text="GET" CssClass="btn-success" OnClick="button2_Click" />
                </td>
                <td class="text-center" style="width: 103px">
                    <asp:Button ID="button4" runat="server" Text="SET" CssClass="dropdown-toggle btn-danger" OnClick="button4_Click" />
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" CssClass="btn"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
