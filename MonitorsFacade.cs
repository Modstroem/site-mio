﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MIO
{
    public class MonitorsFacade
    {
        private static MonitorsFacade instans = new MonitorsFacade();
        public static MonitorsFacade GetInstans() { return instans; }

        public string GetTal(string tab)
        {
            string tal = "0";
            try
            {
                string DGKConnection = "Data Source=SPSQL004;Initial Catalog=MIIS;Persist Security Info=True;User ID=API;password=Duppie36#;";

                SqlConnection conn = new SqlConnection(DGKConnection);

                conn.Open();


                SqlCommand cmd = new SqlCommand("SELECT " + tab + " From [MIIS].[dbo].[AMMonitors]", conn);
                SqlDataReader reader = null;
                System.Data.DataTable dataTable = null;

                reader = cmd.ExecuteReader();
                dataTable = new DataTable();
                dataTable.Load(reader);
                reader.Close();
                conn.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    try
                    {
                        string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                        tal = fields[0].ToString();
                    }
                    catch
                    {
                        tal = "-1";
                    }

                }

                conn.Close();

            }
            catch { tal = "-1"; }

            return tal;
        }

        public string SetTal(string tab, int antal)
        {
            string tal = "0";
            try
            {
                string DGKConnection = "Data Source=SPSQL004;Initial Catalog=MIIS;Persist Security Info=True;User ID=API;password=Duppie36#;";

                SqlConnection conn = new SqlConnection(DGKConnection);

                conn.Open();


                SqlCommand command = new SqlCommand("UPDATE [MIIS].[dbo].[AMMonitors] set " + tab + " = " + antal, conn);
                int r = command.ExecuteNonQuery();
                if (r == 1)
                {
                    tal = antal.ToString();

                }
                else
                {
                    tal = "-1";


                }
                conn.Close();

            }
            catch { tal = "-1"; }

            return tal;
        }

        public string Test = "";
    }
}