﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MIO._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <p class="lead">Modstrøm Internal Overview</p>
        <p><a href="Pages/DGKOverview.aspx" class="btn btn-default">DGK Overview &raquo;</a></p>
        <p><a href="Pages/ltlvp.aspx" class="btn btn-default">Luft til Luft varmepumper &raquo;</a></p>
        <p><a href="Pages/Isolering.aspx" class="btn btn-default">Isolering &raquo;</a></p>
        <p><a href="Pages/dgt.aspx" class="btn btn-default">Den Grønne Timer &raquo;</a></p>
        <p><a href="Pages/OnlyType10.aspx" class="btn btn-default">Kun IKKE strøm kunder &raquo;</a> <span style="font-size: small">(min 5 min loading time)</span></p>
        <p><a href="Pages/ComunicationsLog.aspx" class="btn btn-default">Comunications Log &raquo;</a></p>
        <p><a href="Pages/Supercooler.aspx" class="btn btn-default">Supercooler &raquo;</a></p>
        <p><a href="Pages/Gulvventilator.aspx" class="btn btn-default">Gulvventilator &raquo;</a></p>
      
    </div>

   

   <%-- <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>--%>

</asp:Content>
