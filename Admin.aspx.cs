﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIO
{
    public partial class Admin : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button1_Click(object sender, EventArgs e)
        {
            try
            {

                string re = MonitorsFacade.GetInstans().GetTal("Supercooler");
                if (re == "0" || re == "-1")
                {
                    Label3.Text = "Nope";
                }
                else
                {
                    TextBox1.Text = re;
                }
            }
            catch { Label3.Text = "Nope"; }
        }

        protected void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Label3.Text = "Ok";
                string re = MonitorsFacade.GetInstans().SetTal("Supercooler", Convert.ToInt32(TextBox1.Text));
                if (re == "0" || re == "-1")
                {
                    Label1.Text = "Nope";
                }
                TextBox1.Text = "";
            }
            catch { Label3.Text = "Nope"; }
        }

        protected void button2_Click(object sender, EventArgs e)
        {
            try
            {

                string re = MonitorsFacade.GetInstans().GetTal("Gulvventilator");
                if (re == "0" || re == "-1")
                {
                    Label4.Text = "Nope";
                }
                else
                {
                    TextBox2.Text = re;
                }
            }
            catch { Label4.Text = "Nope"; }
        }

        protected void button4_Click(object sender, EventArgs e)
        {
            try
            {
                Label4.Text = "Ok";
                string re = MonitorsFacade.GetInstans().SetTal("Gulvventilator", Convert.ToInt32(TextBox2.Text));
                if (re == "0" || re == "-1")
                {
                    Label4.Text = "Nope";
                }
                TextBox2.Text = "";

            }
            catch { Label4.Text = "Nope"; }
        }
    }
}